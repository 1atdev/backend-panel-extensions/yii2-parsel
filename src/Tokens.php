<?php

/****************************************************************************************
 *                                                                                      *
 * Yii2 Parsel Query                                                                    *
 * Copyright (c) 2023 PT. IsAtDev. All Rights Reserved.                                 *
 *                                                                                      *
 * ==================================================================================== *
 *                                                                                      *
 * Company       : PT. IsAtDev Digital Innovasi                                         *
 * NIB           : 2111220046069                                                        *
 * Website       : https://www.isatdev.com                                              *
 *                                                                                      *
 * ==================================================================================== *
 *                                                                                      *
 * Author        : Mirdani Handoko                                                      *
 * Email         : dani@emailku.id                                                      *
 * File Name     : Tokens.php                                                           *
 * Update        : Monday, 04 December 2023 13:25:04 WIB                                *
 *                                                                                      *
 ****************************************************************************************/

namespace IsAtDev\Parsel;

use InvalidArgumentException;
use ReflectionClass;

/**
 * A collection of Tokens
 *
 * @see       [[Token]]
 *
 * @author    John Snook
 * @date      2018-07-28
 * @license   https://github.com/johnsnook/yii2-parsel/LICENSE
 * @copyright 2018 John Snook Consulting
 */
final class Tokens
{
    const WHITESPACE = 0;
    const BRACE_OPEN = 1;
    const BRACE_CLOSE = 2;
    const KEYWORD = 3;
    const NEGATION = 4;
    const TERM = 5;
    const TERM_QUOTED = 6;
    const TERM_QUOTED_SINGLE = 7;
    const FIELD_TERM = 8;
    const FIELD_TERM_QUOTED = 9;
    const FIELD_TERM_QUOTED_SINGLE = 10;
    const FULL_MATCH = 11;

    /**
     * Get a token name from its value
     *
     * @param int $token
     *
     * @return string
     */
	public static function getName(int $token): string
	{
        $tokens = self::getTokenMapping();

        if (!isset($tokens[$token])) {
	        throw new InvalidArgumentException(sprintf('Token with value "%d" was not found', $token));
        }

        return $tokens[$token];
    }
	
	/**
	 * I didn't write this and am too tired to figure out what it's doing right now
	 *
	 * @staticvar array $constants
	 * @return array
	 */
	private static function getTokenMapping(): array
	{
		static $constants;
		
		if (is_null($constants)) {
			$reflection = new ReflectionClass(__CLASS__);
			$constants = array_flip($reflection->getConstants());
		}
		
		return $constants;
	}

    /**
     * Get tokens identifying term values
     *
     * @return array
     */
	public static function getTermTokens(): array
	{
        static $termTokens;

        if (null === $termTokens) {
            $termTokens = [
                self::TERM,
                self::TERM_QUOTED,
                self::TERM_QUOTED_SINGLE,
                self::FIELD_TERM,
                self::FIELD_TERM_QUOTED,
                self::FIELD_TERM_QUOTED_SINGLE
            ];
        }

        return $termTokens;
    }

}
