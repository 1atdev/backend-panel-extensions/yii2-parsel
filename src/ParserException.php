<?php

/****************************************************************************************
 *                                                                                      *
 * Yii2 Parsel Query                                                                    *
 * Copyright (c) 2023 PT. IsAtDev. All Rights Reserved.                                 *
 *                                                                                      *
 * ==================================================================================== *
 *                                                                                      *
 * Company       : PT. IsAtDev Digital Innovasi                                         *
 * NIB           : 2111220046069                                                        *
 * Website       : https://www.isatdev.com                                              *
 *                                                                                      *
 * ==================================================================================== *
 *                                                                                      *
 * Author        : Mirdani Handoko                                                      *
 * Email         : dani@emailku.id                                                      *
 * File Name     : ParserException.php                                                  *
 * Update        : Monday, 04 December 2023 13:25:04 WIB                                *
 *                                                                                      *
 ****************************************************************************************/

namespace IsAtDev\Parsel;

use RuntimeException;

/**
 * A Runtime exception by any other name stinks just as bad
 *
 * @author    John Snook
 * @date      2018-07-28
 * @license   https://github.com/johnsnook/yii2-parsel/LICENSE
 * @copyright 2018 John Snook Consulting
 */
final class ParserException extends RuntimeException
{

}
