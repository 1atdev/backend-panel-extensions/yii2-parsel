<?php

/****************************************************************************************
 *                                                                                      *
 * Yii2 Parsel Query                                                                    *
 * Copyright (c) 2023 PT. IsAtDev. All Rights Reserved.                                 *
 *                                                                                      *
 * ==================================================================================== *
 *                                                                                      *
 * Company       : PT. IsAtDev Digital Innovasi                                         *
 * NIB           : 2111220046069                                                        *
 * Website       : https://www.isatdev.com                                              *
 *                                                                                      *
 * ==================================================================================== *
 *                                                                                      *
 * Author        : Mirdani Handoko                                                      *
 * Email         : dani@emailku.id                                                      *
 * File Name     : Parser.php                                                           *
 * Update        : Monday, 04 December 2023 13:25:04 WIB                                *
 *                                                                                      *
 ****************************************************************************************/

namespace IsAtDev\Parsel;

use OverflowException;

/**
 * This class processes the array of tokens generated by [[Lexer::lex]]
 *
 * @author    John Snook
 * @date      2018-07-28
 * @license   https://github.com/johnsnook/yii2-parsel/LICENSE
 * @copyright 2018 John Snook Consulting
 */
final class Parser
{
	/**
	 * @var null No quotes
	 */
	const QUOTE_NONE = null;
	
	/**
	 * @var string Single quotes
	 */
	const QUOTE_SINGLE = "'";
	
	/**
	 * @var string Double quotes
	 */
	const QUOTE_DOUBLE = '"';
	
	/**
	 * @param array $parts
	 * @param int   $index
	 *
	 * @return mixed
	 */
	protected static function getPart(array $parts, int $index): mixed
	{
		if (isset($parts[$index])) {
			return $parts[$index];
		}
		
		throw new OverflowException('Invalid part index');
	}
	
	/**
	 * This is the main event for this class.  Given the tokens generated by
	 * [[Lexer::lex]], it
	 *
	 * @param Token[] $tokens The token lexemes from Lexer.php
	 *
	 * @return array An array representing the terms, conjunctions and subqueries and their properties
	 */
	public function parse(array $tokens): array
	{
		self::sanityCheck($tokens);
		
		$currentQuery = [];
		
		/** @var Token $previousToken */
		$previousToken = null;
		
		for ($i = 0; $i < count($tokens); $i++) {
			$token = $tokens[$i];
			
			// brace open/close - sub-queries
			if ($token->isTypeOf(Tokens::BRACE_OPEN)) {
				$negated = self::isNegated($i, $tokens);
				$currentQuery = self::conjunctionJunction($currentQuery);
				$braceClosePos = self::findMatchPos($tokens, $i);
				$subwayTokens = array_slice($tokens, $i + 1, $braceClosePos - $i - 1);
				
				$currentQuery[] = [
					'type' => 'query',
					'negated' => $negated,
					'items' => $this->parse($subwayTokens),
				];
				$i = $braceClosePos;
				
				continue;
			}
			
			/** terms (the actual values we're looking for) */
			if (in_array($token->type, Tokens::getTermTokens())) {
				$term = [
					'type' => 'term',
					'value' => self::normalizeTerm($token),
					'fuzzy' => self::isFuzzy($token),
					'negated' => self::isNegated($i, $tokens),
					'fullMatch' => self::isFullMatch($i, $tokens),
					'quoted' => self::quoteType($token),
				];
				
				/** If token has a field, add it to array part attribute */
				if ($token->isTypeOf([
					Tokens::FIELD_TERM,
					Tokens::FIELD_TERM_QUOTED,
					Tokens::FIELD_TERM_QUOTED_SINGLE,
				])) {
					$term['field'] = $token->field;
				}
				$currentQuery = self::conjunctionJunction($currentQuery);
				$currentQuery[] = $term;
			} // terms
			
			/** Keywords */
			if ($token->isTypeOf(Tokens::KEYWORD)) {
				if ($previousToken && $previousToken->isTypeOf(Tokens::KEYWORD)) {
					throw new ParserException(sprintf(
						'Keyword can\'t be succeeded by another keyword (%s %s)', $previousToken->value, $token->value
					));
				}
				$currentQuery[] = ['type' => 'keyword', 'value' => strtoupper($token->value)];
			}
			
			$previousToken = $token;
		}
		
		return $currentQuery;
	}
	
	/**
	 * Check to see if all braces are balanced
	 *
	 * @param array $tokens
	 *
	 * @throws ParserException
	 */
	protected static function sanityCheck(array $tokens): void
	{
		$left = $right = 0;
		
		foreach ($tokens as $token) {
			$left += $token->name === 'BRACE_OPEN' ? 1 : 0;
			$right += $token->name === 'BRACE_CLOSE' ? 1 : 0;
		}
		
		if ($left !== $right) {
			throw new ParserException("The query has $left left braces and $right right braces.");
		}
	}
	
	/**
	 * Check if expression was negated by looking back at previous tokens
	 *
	 * @param int     $index
	 * @param Token[] $tokens
	 *
	 * @return bool
	 */
	protected static function isNegated(int $index, array $tokens): bool
	{
		$negated = false;
		
		$startIndex = $index - 1;
		if ($startIndex < 0) {
			return false;
		}
		
		for ($i = $startIndex; $i >= 0; $i--) {
			if ($tokens[$i]->isTypeOf(Tokens::NEGATION)) {
				$negated = !$negated;
			} else {
				break;
			}
		}
		
		return $negated;
	}
	
	/**
	 * Add an AND/OR before inserting the term if the last part was no keyword
	 *
	 * @param array $currentQuery
	 *
	 * @return array
	 */
	protected static function conjunctionJunction(array $currentQuery): array
	{
		$lastPart = self::getLastPart($currentQuery);
		
		if (isset($lastPart['type']) && ($lastPart['type'] !== 'keyword')) {
			$currentQuery[] = ['type' => 'keyword', 'value' => 'AND'];
		}
		
		return $currentQuery;
	}
	
	/**
	 * Used to look one element back in the parts array
	 */
	protected static function getLastPart($parts)
	{
		if (empty($parts)) {
			return [];
		}
		
		$keys = array_keys($parts);
		
		return $parts[$keys[count($keys) - 1]];
	}
	
	/**
	 * Returns the position of the matching brace
	 *
	 * @param array   $tokens
	 * @param integer $i
	 *
	 * @return int|void
	 */
	protected static function findMatchPos(array $tokens, int $i)
	{
		$j = 0;
		
		for ($i; $i <= count($tokens) + 1; $i++) {
			$j += ($tokens[$i]->type === Tokens::BRACE_OPEN ? 1 : 0);
			if ($tokens[$i]->type === Tokens::BRACE_CLOSE) {
				if (--$j < 1) {
					return $i;
				}
			}
		}
	}
	
	/**
	 * Normalize term (strip quotes)
	 *
	 * @param Token $token
	 *
	 * @return string
	 */
	protected static function normalizeTerm(Token $token): string
	{
		$term = $token->value;
		
		if ($token->isTypeOf([Tokens::TERM_QUOTED, Tokens::FIELD_TERM_QUOTED])) {
			$term = preg_replace('/^"(.*)"$/', '$1', $term);
		} else if ($token->isTypeOf([Tokens::TERM_QUOTED_SINGLE, Tokens::FIELD_TERM_QUOTED_SINGLE])) {
			$term = preg_replace('/^\'(.*)\'$/', '$1', $term);
		}
		
		return $term;
	}
	
	/**
	 * Check if the token is fuzzy (is not single quoted and contains *)
	 *
	 * @param Token $token
	 *
	 * @return bool
	 */
	protected static function isFuzzy(Token $token): bool
	{
		if ($token->isTypeOf([Tokens::TERM_QUOTED_SINGLE])) {
			return false;
		}
		
		return false !== (strpos($token->value, '*') || strpos($token->value, '?'));
	}
	
	/**
	 * Check if expression is a full match by looking back at previous tokens
	 *
	 * @param int     $index
	 * @param Token[] $tokens
	 *
	 * @return bool
	 */
	protected static function isFullMatch(int $index, array $tokens): bool
	{
		$fullMatch = false;
		
		$startIndex = $index - 1;
		if ($startIndex < 0) {
			return false;
		}
		
		for ($i = $startIndex; $i >= 0; $i--) {
			if ($tokens[$i]->isTypeOf(Tokens::FULL_MATCH)) {
				$fullMatch = !$fullMatch;
			} else {
				break;
			}
		}
		
		return $fullMatch;
	}
	
	/**
	 * Check if the token is fuzzy (is not single quoted and contains *)
	 *
	 * @param Token $token
	 *
	 * @return string|null
	 */
	protected static function quoteType(Token $token): ?string
	{
		if ($token->isTypeOf([Tokens::TERM_QUOTED, Tokens::FIELD_TERM_QUOTED])) {
			return self::QUOTE_DOUBLE;
		}
		
		if ($token->isTypeOf([Tokens::TERM_QUOTED_SINGLE, Tokens::FIELD_TERM_QUOTED_SINGLE])) {
			return self::QUOTE_SINGLE;
		}
		
		return self::QUOTE_NONE;
	}
	
}
