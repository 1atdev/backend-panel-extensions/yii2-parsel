<?php

declare(strict_types=1);

namespace IsAtDev\Parsel\Libs;

interface LexerFactory
{
	public function createLexer(array $lexerDefinition, string $additionalModifiers = ''): Lexer;
}