<?php

namespace IsAtDev\Parsel\Libs\LexerFactory\Stateless;

use IsAtDev\Parsel\Libs\Lexer;

class WithoutCapturingGroups implements \IsAtDev\Parsel\Libs\LexerFactory
{
	protected $dataGen;
	
	public function __construct(\IsAtDev\Parsel\Libs\LexerDataGenerator $dataGen)
	{
		$this->dataGen = $dataGen;
	}
	
	public function createLexer(array $lexerDefinition, string $additionalModifiers = ''): Lexer
	{
		$regexes = array_keys($lexerDefinition);
		
		$compiledRegex = $this->dataGen->getCompiledRegex($regexes, $additionalModifiers);
		$offsetToLengthMap = $this->dataGen->getOffsetToLengthMap($regexes);
		$offsetToTokenMap = array_combine(array_keys($offsetToLengthMap), $lexerDefinition);
		
		return new \IsAtDev\Parsel\Libs\Lexer\Stateless\WithoutCapturingGroups(
			$compiledRegex, $offsetToTokenMap
		);
	}
}