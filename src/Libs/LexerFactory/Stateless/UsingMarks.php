<?php

namespace IsAtDev\Parsel\Libs\LexerFactory\Stateless;

use IsAtDev\Parsel\Libs\Lexer;

class UsingMarks implements \IsAtDev\Parsel\Libs\LexerFactory
{
	protected $dataGen;
	
	public function __construct(\IsAtDev\Parsel\Libs\LexerDataGenerator $dataGen)
	{
		$this->dataGen = $dataGen;
	}
	
	public function createLexer(array $lexerDefinition, string $additionalModifiers = ''): Lexer
	{
		$regexes = array_keys($lexerDefinition);
		$marks = $this->dataGen->getMarks(count($regexes));
		$compiledRegex = $this->dataGen->getCompiledRegexWithMarks($regexes, $marks, $additionalModifiers);
		$markToTokenMap = array_combine($marks, $lexerDefinition);
		
		return new \IsAtDev\Parsel\Libs\Lexer\Stateless\UsingMarks(
			$compiledRegex, $markToTokenMap
		);
	}
}