<?php

namespace IsAtDev\Parsel\Libs\LexerFactory\Stateless;

use IsAtDev\Parsel\Libs\Lexer;

class Simple implements \IsAtDev\Parsel\Libs\LexerFactory
{
	public function createLexer(array $lexerDefinition, string $additionalModifiers = ''): Lexer
	{
		return new \IsAtDev\Parsel\Libs\Lexer\Stateless\Simple($lexerDefinition, $additionalModifiers);
	}
}