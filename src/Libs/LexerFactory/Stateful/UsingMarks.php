<?php

namespace IsAtDev\Parsel\Libs\LexerFactory\Stateful;

use IsAtDev\Parsel\Libs\Lexer;

class UsingMarks implements \IsAtDev\Parsel\Libs\LexerFactory
{
	protected $dataGen;
	
	public function __construct(\IsAtDev\Parsel\Libs\LexerDataGenerator $dataGen)
	{
		$this->dataGen = $dataGen;
	}
	
	public function createLexer(array $lexerDefinition, string $additionalModifiers = ''): Lexer
	{
		$initialState = key($lexerDefinition);
		
		$stateData = [];
		foreach ($lexerDefinition as $state => $regexToActionMap) {
			$regexes = array_keys($regexToActionMap);
			$marks = $this->dataGen->getMarks(count($regexes));
			
			$compiledRegex = $this->dataGen->getCompiledRegexWithMarks($regexes, $marks, $additionalModifiers);
			$markToActionMap = array_combine($marks, $regexToActionMap);
			
			$stateData[$state] = [
				'compiledRegex' => $compiledRegex,
				'markToActionMap' => $markToActionMap,
			];
		}
		
		return new \IsAtDev\Parsel\Libs\Lexer\Stateful\UsingMarks($initialState, $stateData);
	}
}