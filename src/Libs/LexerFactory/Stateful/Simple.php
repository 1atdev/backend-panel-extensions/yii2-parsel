<?php

namespace IsAtDev\Parsel\Libs\LexerFactory\Stateful;

use IsAtDev\Parsel\Libs\Lexer;

class Simple implements \IsAtDev\Parsel\Libs\LexerFactory
{
	public function createLexer(array $lexerDefinition, string $additionalModifiers = ''): Lexer
	{
		$initialState = key($lexerDefinition);
		
		return new \IsAtDev\Parsel\Libs\Lexer\Stateful\Simple($initialState, $lexerDefinition, $additionalModifiers);
	}
}