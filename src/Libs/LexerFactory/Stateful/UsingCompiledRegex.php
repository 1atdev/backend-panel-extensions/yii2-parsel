<?php

namespace IsAtDev\Parsel\Libs\LexerFactory\Stateful;

use IsAtDev\Parsel\Libs\Lexer;

class UsingCompiledRegex implements \IsAtDev\Parsel\Libs\LexerFactory
{
	protected $dataGen;
	
	public function __construct(\IsAtDev\Parsel\Libs\LexerDataGenerator $dataGen)
	{
		$this->dataGen = $dataGen;
	}
	
	public function createLexer(array $lexerDefinition, string $additionalModifiers = ''): Lexer
	{
		$initialState = key($lexerDefinition);
		
		$stateData = [];
		foreach ($lexerDefinition as $state => $regexToActionMap) {
			$regexes = array_keys($regexToActionMap);
			
			$compiledRegex = $this->dataGen->getCompiledRegex($regexes, $additionalModifiers);
			$offsetToLengthMap = $this->dataGen->getOffsetToLengthMap($regexes);
			$offsetToActionMap = array_combine(array_keys($offsetToLengthMap), $regexToActionMap);
			
			$stateData[$state] = [
				'compiledRegex' => $compiledRegex,
				'offsetToActionMap' => $offsetToActionMap,
				'offsetToLengthMap' => $offsetToLengthMap,
			];
		}
		
		return new \IsAtDev\Parsel\Libs\Lexer\Stateful\UsingCompiledRegex($initialState, $stateData);
	}
}