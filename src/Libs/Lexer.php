<?php

declare(strict_types=1);

namespace IsAtDev\Parsel\Libs;

interface Lexer
{
	public function lex(string $string): array;
}