<?php

declare(strict_types=1);

namespace IsAtDev\Parsel\Libs;

class LexingException extends \RuntimeException { }