<?php

namespace IsAtDev\Parsel\Libs\Lexer\Stateless;

class WithCapturingGroups implements \IsAtDev\Parsel\Libs\Lexer
{
	protected $compiledRegex;
	protected $offsetToTokenMap;
	protected $offsetToLengthMap;
	
	public function __construct(string $compiledRegex, array $offsetToTokenMap, array $offsetToLengthMap)
	{
		$this->compiledRegex = $compiledRegex;
		$this->offsetToTokenMap = $offsetToTokenMap;
		$this->offsetToLengthMap = $offsetToLengthMap;
	}
	
	public function lex(string $string): array
	{
		$tokens = [];
		
		$offset = 0;
		$line = 1;
		while (isset($string[$offset])) {
			if (!preg_match($this->compiledRegex, $string, $matches, 0, $offset)) {
				throw new \IsAtDev\Parsel\Libs\LexingException(sprintf(
					'Unexpected character "%s" on line %d', $string[$offset], $line
				));
			}
			
			// find the first non-empty element (but skipping $matches[0]) using a quick for loop
			for ($i = 1; '' === $matches[$i]; ++$i) ;
			
			$realMatches = [];
			for ($j = 1, $length = $this->offsetToLengthMap[$i - 1]; $j < $length; ++$j) {
				if (isset($matches[$i + $j])) {
					$realMatches[$j] = $matches[$i + $j];
				}
			}
			
			if (!empty($realMatches)) {
				$tokens[] = [$this->offsetToTokenMap[$i - 1], $line, $matches[0], $realMatches];
			} else {
				$tokens[] = [$this->offsetToTokenMap[$i - 1], $line, $matches[0]];
			}
			
			$offset += strlen($matches[0]);
			$line += substr_count($matches[0], "\n");
		}
		
		return $tokens;
	}
}