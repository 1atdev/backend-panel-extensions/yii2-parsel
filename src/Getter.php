<?php

/****************************************************************************************
 *                                                                                      *
 * Yii2 Parsel Query                                                                    *
 * Copyright (c) 2023 PT. IsAtDev. All Rights Reserved.                                 *
 *                                                                                      *
 * ==================================================================================== *
 *                                                                                      *
 * Company       : PT. IsAtDev Digital Innovasi                                         *
 * NIB           : 2111220046069                                                        *
 * Website       : https://www.isatdev.com                                              *
 *                                                                                      *
 * ==================================================================================== *
 *                                                                                      *
 * Author        : Mirdani Handoko                                                      *
 * Email         : dani@emailku.id                                                      *
 * File Name     : Getter.php                                                           *
 * Update        : Monday, 04 December 2023 13:25:04 WIB                                *
 *                                                                                      *
 ****************************************************************************************/

namespace IsAtDev\Parsel;

use yii\base\InvalidCallException;
use yii\base\UnknownPropertyException;

/**
 * A class to wrap php __get magic method
 *
 * @author    John Snook
 * @date      2018-07-28
 * @license   https://github.com/johnsnook/yii2-parsel/LICENSE
 * @copyright 2018 John Snook Consulting
 */
class Getter
{
    /**
     * Returns the value of an object property.
     *
     * Do not call this method directly as it is a PHP magic method that
     * will be implicitly called when executing `$value = $object->property;`.
     * @param string $name the property name
     * @return mixed the property value
     * @throws UnknownPropertyException if the property is not defined
     * @throws InvalidCallException if the property is write-only
     */
	public function __get(string $name)
	{
        $getter = 'get' . $name;
		
		if (method_exists($this, $getter)) {
            return $this->$getter();
        } elseif (method_exists($this, 'set' . $name)) {
            throw new InvalidCallException('Getting write-only property: ' . get_class($this) . '::' . $name);
        }

        throw new UnknownPropertyException('Getting unknown property: ' . get_class($this) . '::' . $name);
    }

}
