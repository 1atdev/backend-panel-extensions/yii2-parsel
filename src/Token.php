<?php

/****************************************************************************************
 *                                                                                      *
 * Yii2 Parsel Query                                                                    *
 * Copyright (c) 2023 PT. IsAtDev. All Rights Reserved.                                 *
 *                                                                                      *
 * ==================================================================================== *
 *                                                                                      *
 * Company       : PT. IsAtDev Digital Innovasi                                         *
 * NIB           : 2111220046069                                                        *
 * Website       : https://www.isatdev.com                                              *
 *                                                                                      *
 * ==================================================================================== *
 *                                                                                      *
 * Author        : Mirdani Handoko                                                      *
 * Email         : dani@emailku.id                                                      *
 * File Name     : Token.php                                                            *
 * Update        : Monday, 04 December 2023 13:25:04 WIB                                *
 *                                                                                      *
 ****************************************************************************************/

namespace IsAtDev\Parsel;

/**
 * Represents a single lexeme token
 *
 * @property-read int $type The type of token as defined in [[Tokens]]
 * @property-read string $name Human readable type name
 * @property-read string $field If token has field definition, the fields name
 * @property-read int $line Not sure what this is for
 * @property-read string $value The tokens contents
 *
 * @author    John Snook
 * @date      2018-07-28
 * @license   https://github.com/johnsnook/yii2-parsel/LICENSE
 * @copyright 2018 John Snook Consulting
 */
final class Token extends Getter
{
    /**
     * @var int
     */
	private int $type;

    /**
     * @var string
     */
	private string $name;

    /**
     * @var string
     */
	private string $field;

    /**
     * @var int
     */
	private int $line;

    /**
     * @var string
     */
	private string $value;

    /**
     * @param int $type
     * @param int $line
     * @param string $value
     */
	public function __construct(int $type, int $line, string $value)
	{
        $this->type = $type;
        $this->line = $line;
        $this->value = $value;

        /**
         * 1) See if a colon is in the term
         * 2) Split the term and set the field to the leftmost split term value
         * 3) set the value to the right side of the original term
         */
        if ($this->isTypeOf([
	        Tokens::FIELD_TERM,
	        Tokens::FIELD_TERM_QUOTED,
	        Tokens::FIELD_TERM_QUOTED_SINGLE,
        ])) {
	        [$this->field, $this->value] = explode(':', $this->value);
        }
        $this->name = Tokens::getName($type);
    }

    /**
     * @param array|int $token
     *
     * @return bool
     */
	public function isTypeOf(array|int $token): bool
	{
        if (!is_array($token)) {
            $token = [$token];
        }

        return in_array($this->type, $token);
    }

    /**
     * @return string
     */
	public function getName(): string
	{
        return $this->name;
    }
	
	/**
	 * @return int
	 */
	public function getType(): int
	{
		return $this->type;
	}

    /**
     * @return string
     */
	public function getField(): string
	{
        return $this->field;
    }

    /**
     * @return int
     */
	public function getLine(): int
	{
        return $this->line;
    }

    /**
     * @return string
     */
	public function getValue(): string
	{
        return $this->value;
    }

}
